const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(express.urlencoded({
  extended: true
}))


app.get('/', (req, res) => {
  res.render('index.html');
})

app.post('/enter-room', (req, res) => {
  const username = req.body.username;
  const roomName = req.body.room_name;
  res.render('room', { room: roomName, username: username })
})
app.get('/enter-room', (req, res) => {
  res.redirect('/');
})


io.on('connection', socket => {
  socket.on('join-room', (roomId, userId) => {
    socket.join(roomId)
    socket.to(roomId).broadcast.emit('user-connected', userId)

    socket.on('disconnect', () => {
      socket.to(roomId).broadcast.emit('user-disconnected', userId)
    })

  })
  socket.on('chat_message', (msg, room, username) => {
    io.to(room).emit('chat_message', msg, username);
  });
})

http.listen(3000)
